<?php
//var_export
echo 'This is var_export() function';
$arr1 = array(1,2,3,4,5);
echo "<pre>";
var_export($arr1);
echo "</pre>";
echo "<br>";

//var_dump
echo 'This is var_dump() function';
echo "<br>";
$arr1 = array(1,2,3,4,5);
echo "<pre>";
var_dump($arr1);
echo "</pre>";
echo "<br>";

//var_export

$name = array("arif","kamal","hasan","sahed","shoeb");
echo "<pre>";
var_export($name);

$name2 = array (
    0 => 'arif',
    1 => 'kamal',
    2 => 'hasan',
    3 => 'sahed',
    4 => 'shoeb',
);
echo "<pre>";
var_export($name2);

//boolval function
echo "\r";
echo '0:        '.(boolval(0) ? 'true' : 'false')."\n";
echo '42:       '.(boolval(42) ? 'true' : 'false')."\n";
echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."\n";
echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."\n";
echo '"":       '.(boolval("") ? 'true' : 'false')."\n";
echo '"string": '.(boolval("string") ? 'true' : 'false')."\n";
echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n";

echo "\n";

echo __FILE__;
echo "\n";

echo __LINE__;
echo "\n";

echo __FUNCTION__;
echo "\n";

echo __DIR__;
echo "\n";

echo __NAMESPACE__;