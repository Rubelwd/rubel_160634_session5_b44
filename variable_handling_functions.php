<?php
//floatval function
$myVar = "3542.215 hello world";
echo floatval($myVar)."<br>";


$myVar = "hello 3542.21 world";
echo floatval($myVar)."<br>";


$myVar = "hello world 3542.21";
echo floatval($myVar)."<br>";



//empty function
$myVar = "";
if(empty($myVar)) {
    echo "$myVar is empty";
} else {
    echo "$myVar is not empty";
}

echo "<br>";

//serialize

$myVar = array("jamal","kamal","sahed","sarif","jabed");
$serializeString = serialize($myVar);
echo $serializeString."<br>";

//Unserialize

$unSerialize = unserialize($serializeString);
echo "<pre>";
var_dump($unSerialize);

//isset/unset

if(isset($unSerialize)) {
    echo "the variable is set";
} else {
    echo "The variable is not set";
}

echo "\n";

unset($unSerialize);
if(isset($unSerialize)) {
    echo "The variable is set";
} else {
    echo "The variable is not set";
}

echo "\n";

$result = boolval("this is a string");
echo "<pre>";
var_dump($result);
echo "<br>";


//boolval function
echo "\r";
echo '0:        '.(boolval(0) ? 'true' : 'false')."\n";
echo '42:       '.(boolval(42) ? 'true' : 'false')."\n";
echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."\n";
echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."\n";
echo '"":       '.(boolval("") ? 'true' : 'false')."\n";
echo '"string": '.(boolval("string") ? 'true' : 'false')."\n";
echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n";

