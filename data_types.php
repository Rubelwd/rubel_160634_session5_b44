<?php

//Starts boolean example
$decision = 1;
if($decision) {
    echo "The decision is yes";
    echo "<br>";
}

$decision = 0;
if($decision == false) {
    echo "The decision is no";
    echo "<br><hr>";
}
//End boolean example



//Starts integer example
$number = 100;
echo "Integer Value: ".$number."<br><hr>";



//Starts floot/double example
$number = 100.235;
echo "Float Value: ".$number."<br><hr>";


//Starts string type example
$number = 100;
$str1 = "This is a double quoted string $number";
echo $str1."<br>";
$str1 = 'This is a single quoted string $number';
echo $str1."<br><hr>";

//Heredoc
$new = <<<BITM
This is heredoc string syntax.
There can be included string with syntax,text,character and many much.
<b>This is html bold tag</b>.This also include escape character like.
I can do it but you can't do it. In such way this is line nember $number
BITM;
echo "<b><u>This is heredoc string:</u></b> ".$new."<br><br>";


//Nowdoc
$new = <<<'BITM'
This is heredoc string syntax.
There can be included string with syntax,text,character and many much.
<b>This is html bold tag</b>.This also include escape character like.
I can do it but you can't do it.In such way this is line nember $number
BITM;
echo "<b><u>This is nowdoc string:</u></b> ".$new."<br><br><hr>";


//Starts array example
//Indexed Array
$arr1 = array(1,2,3,4,5,6,7,8,9,10);
echo "<pre>";
print_r($arr1)."<br>";

$car = array("BMW","Ford","AUDI","FERRARI","TOYOTA","LUMIGIN","TATA");
echo "<pre>";
print_r($car);

//Associative Array
$age = array(
    "Arif"  => 30,
    "Sakib" => 15,
    "Arman" => 35,
    "Jabed" => 26,
    "Sohel" => 20
);
echo "<pre>";
print_r($age)."<br>";


echo "Arif's age is : ".$age['Arif']."<hr>";


