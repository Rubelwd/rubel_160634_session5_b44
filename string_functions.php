<?php
//String Functions

echo "How are \"you\" !"."<br>";
echo 'How are "You"'."<br>";
echo "How are 'You'"."<br>";
$myStr = addslashes('Hello "37" ');
echo $myStr;
echo "<br>";

echo stripcslashes($myStr);
echo "<br><br>";


//Explode and Implode
echo "<u><b>Explode Function</b></u>";
$myStr = "I love my country Bangladesh";
$newStr = explode(" ", $myStr,5);
echo "<pre>";
print_r($newStr);
echo "<br>";


echo "<u><b>Implode Function</b></u> <br>";
$new = implode(" ", $newStr);
echo $new;
echo "<br>";

//htmlentities
//echo "<br> means line break";
echo htmlentities("<br> means line break");
echo "<br>";

//trim functions

$str1 = " Our country name is Bangladesh ";
$str1 = trim($str1);
echo $str1."<br>";

$str1 = " Our country name is Bangladesh ";
$str1 = ltrim($str1);
echo $str1."<br>";

$str1 = " Our country name is Bangladesh ";
$str1 = rtrim($str1);
echo $str1."<br>";

//nl2br Functions
$myStr = "\n\n Hello world";
echo nl2br($myStr);
echo "<br>";

//str_pad functions
$new1 = "Bangladesh";
$new2 = str_pad($new1,25,".");
echo $new2;
echo "<br>";


//Str_repeat
$new3 = "Ok ";
$new3 = str_repeat($new3,15);
echo $new3."<br>";

//str_replace

$new4 = "This is a new string";
echo "<u><b>Before replace:</b></u> $new4 <br>";
$new4 = str_replace("new","another",$new4);
echo $new4."<br>";

//str_split Functions
$new5 = "string";
$new5 = str_split($new5);
print_r($new5);

//strlen Functions
$new6 = "This is a new string";
$new6 = strlen($new6);
echo $new6."<br>";

//strtolower Functions
$new7 = "this is a lowercase string";
$new7 = strtolower($new7);
echo $new7."<br>";

//strtoupper Functions
$new8 = "this is a uppercase string";
$new8 = strtoupper($new8);
echo $new8."<br>";

//substr_compare
$str1 = "my name is rubel";
$str2 = "my name is";
$str3 = substr_compare($str1,$str2,0);
echo $str3;

//substr_count Functions
$str8 = "What is your name? My name is Rubel";
$str8 = substr_count($str8,"is");
echo $str8."<br>";

//substr_replace Functions
$str8 = "What is your name? My name is Rubel.";
$str8 = substr_replace($str8,"sahed",30);
echo $str8."<br>";

//ucfirst Functions
$str = 'my nama is Rubel';
$str = ucfirst($str);
echo $str;



